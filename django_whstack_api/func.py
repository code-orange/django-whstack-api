from django.db.models import Q

from django_mdat_customer.django_mdat_customer.models import MdatCustomers
from django_powerdns_models.django_powerdns_models.models import Domains
from django_whstack_models.django_whstack_models.models import WhRegistryDomains


def get_own_dns_zones(customer: MdatCustomers):
    all_customer_domains = WhRegistryDomains.objects.filter(
        customer=customer,
    )

    own_dns_zones = list()

    for domain in all_customer_domains:
        for zone in Domains.objects.filter(
            Q(name=domain.domain) | Q(name__endswith="." + domain.domain)
        ):
            own_dns_zones.append(zone.name.lower())

    return own_dns_zones


def get_own_dns_templates(customer: MdatCustomers):
    own_dns_templates = list()

    all_customer_templates = Domains.objects.filter(
        name__startswith=str(customer.id) + "-",
    ).filter(
        name__endswith=".zone-templates.local",
    )

    for zone in all_customer_templates:
        own_dns_templates.append(zone.name.lower())

    return own_dns_templates
