from django.urls import path, include
from tastypie.api import Api

from django_tastypie_generalized_api_auth.django_tastypie_generalized_api_auth.api.resources import *
from django_whstack_api.django_whstack_api.api.resources import *

v1_api = Api(api_name="v1")

# GENERAL
v1_api.register(ApiAuthCustomerApiKeyResource())
v1_api.register(ApiAuthCustomerResource())

# ACME
v1_api.register(AcmeCertPushResource())
v1_api.register(AcmeCertResource())

# CERTIFICATE
v1_api.register(CertificateResource())

# HOST
v1_api.register(HostResource())

# HTTPFRONT
v1_api.register(HttpFrontResource())

# CLUSTER
v1_api.register(ClusterResource())

# DNS
v1_api.register(DnsZoneResource())
v1_api.register(DnsZoneTemplateResource())

# WEBSITE
v1_api.register(WebsiteResource())

# DOMAIN
v1_api.register(RegistryDomainResource())
v1_api.register(TldResource())

urlpatterns = [
    path("", include(v1_api.urls)),
]
