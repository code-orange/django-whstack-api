from urllib import parse

from django.forms import model_to_dict
from tastypie import fields
from tastypie.exceptions import BadRequest, Unauthorized, UnsupportedFormat
from tastypie.resources import Resource
from tastypie.serializers import Serializer

from django_powerdns_models.django_powerdns_models.models import Domains, Domainmetadata
from django_simple_notifier.django_simple_notifier.plugin_zammad import (
    send as zammad_send,
)
from django_tastypie_generalized_api_auth.django_tastypie_generalized_api_auth.authentification import (
    ApiAuthCustomerAuthentication,
)
from django_whstack_api.django_whstack_api.func import (
    get_own_dns_zones,
    get_own_dns_templates,
)
from django_whstack_main.django_whstack_main.func import *
from django_whstack_models.django_whstack_models.models import *
from django_whstack_pdns.django_whstack_pdns.func import *
from django_whstack_rrpproxy.django_whstack_rrpproxy.func import *


class UrlencodeSerializer(Serializer):
    formats = ["json", "jsonp", "xml", "yaml", "html", "plist", "urlencode"]
    content_types = {
        "json": "application/json",
        "jsonp": "text/javascript",
        "xml": "application/xml",
        "yaml": "text/yaml",
        "html": "text/html",
        "plist": "application/x-plist",
        "urlencode": "application/x-www-form-urlencoded",
    }

    def from_urlencode(self, data, options=None):
        """handles basic formencoded url posts"""
        qs = dict(
            (k, v if len(v) > 1 else v[0]) for k, v in parse.parse_qs(data).items()
        )
        return qs

    def to_urlencode(self, content):
        pass


class HttpFrontResource(Resource):
    class Meta:
        queryset = WhWebsites.objects.all()
        limit = 0
        max_limit = 0
        always_return_data = True
        allowed_methods = ["get"]
        serializer = Serializer()
        authentication = ApiAuthCustomerAuthentication()

    def get_resource_uri(self, bundle_or_obj=None, url_name="api_dispatch_list"):
        return

    def obj_get(self, bundle, **kwargs):
        try:
            bundle.obj = WhClusters.objects.get(id=kwargs["pk"])
        except WhClusters.DoesNotExist:
            return

        return bundle

    def dehydrate(self, bundle):
        if bundle.request.method == "GET":
            bundle.data = dict()

            for website in list(bundle.obj.obj.whwebsites_set.all()):
                bundle.data[website.id] = dict()
                bundle.data[website.id]["domains"] = dict()
                bundle.data[website.id]["params"] = get_web_vars(website)
                bundle.data[website.id]["httpfront_nginx"] = get_httpfront_nginx(
                    website
                )

                for domain in website.whdomains_set.all():
                    bundle.data[website.id]["domains"][domain.id] = get_dom_vars(domain)

                if len(bundle.data[website.id]["domains"]) <= 0:
                    continue

        return bundle


class CertificateResource(Resource):
    class Meta:
        queryset = WhWebsites.objects.all()
        limit = 0
        max_limit = 0
        always_return_data = True
        allowed_methods = ["get"]
        serializer = Serializer()
        authentication = ApiAuthCustomerAuthentication()

    def get_resource_uri(self, bundle_or_obj=None, url_name="api_dispatch_list"):
        return

    def obj_get(self, bundle, **kwargs):
        try:
            bundle.obj = WhClusters.objects.get(id=kwargs["pk"])
        except WhClusters.DoesNotExist:
            return

        return bundle

    def dehydrate(self, bundle):
        if bundle.request.method == "GET":
            bundle.data = dict()

            for website in list(bundle.obj.obj.whwebsites_set.all()):
                bundle.data[website.id] = dict()
                bundle.data[website.id]["domains"] = dict()
                bundle.data[website.id]["params"] = get_web_vars(website)
                bundle.data[website.id]["httpfront_nginx"] = get_httpfront_nginx(
                    website
                )

                for domain in website.whdomains_set.all():
                    bundle.data[website.id]["domains"][domain.id] = get_dom_vars(domain)

                if len(bundle.data[website.id]["domains"]) <= 0:
                    continue

        return bundle


class AcmeCertResource(Resource):
    class Meta:
        queryset = WhWebsites.objects.all()
        limit = 0
        max_limit = 0
        always_return_data = True
        allowed_methods = ["get"]
        serializer = Serializer()
        authentication = ApiAuthCustomerAuthentication()

    def get_resource_uri(self, bundle_or_obj=None, url_name="api_dispatch_list"):
        return

    def obj_get(self, bundle, **kwargs):
        try:
            bundle.obj = WhClusters.objects.get(id=kwargs["pk"])
        except WhClusters.DoesNotExist:
            return

        return bundle

    def dehydrate(self, bundle):
        if bundle.request.method == "GET":
            bundle.data = dict()

            for website in list(bundle.obj.obj.whwebsites_set.all()):
                website_vars = get_web_vars(website)

                if "tls_renew" in website_vars:
                    if website_vars["tls_renew"] != "true":
                        continue
                else:
                    continue

                if len(website.whdomains_set.all()) < 1:
                    continue

                bundle.data[website.id] = dict()
                bundle.data[website.id]["domains"] = list()
                bundle.data[website.id]["acmecert_certbot_config"] = (
                    get_acmecert_certbot_config(website)
                )

                if "tls_source" in website_vars:
                    bundle.data[website.id]["tls_source"] = website_vars["tls_source"]

                for domain in website.whdomains_set.all():
                    bundle.data[website.id]["domains"].extend(get_dom_vars(domain))

                if len(bundle.data[website.id]["domains"]) <= 0:
                    continue

        return bundle


class AcmeCertPushResource(Resource):
    website_id = fields.IntegerField(attribute="website_id")
    data_fullchain = fields.CharField(attribute="data_fullchain")
    data_privkey = fields.CharField(attribute="data_privkey")

    class Meta:
        queryset = WhWebsites.objects.all()
        limit = 0
        max_limit = 0
        allowed_methods = ["post"]
        authentication = ApiAuthCustomerAuthentication()
        serializer = UrlencodeSerializer()

    def obj_create(self, bundle, **kwargs):
        bundle.related_obj = WhWebsites.objects.get(id=bundle.data["website_id"])
        bundle.obj = WhWebsites.objects.get(id=bundle.data["website_id"])

        try:
            data_fullchain = WhWebsiteVars.objects.get(
                website=bundle.related_obj.id, name="tls_cert"
            )
        except WhWebsiteVars.DoesNotExist:
            data_fullchain = WhWebsiteVars(website=bundle.related_obj, name="tls_cert")

        data_fullchain.value = bundle.data["data_fullchain"]
        data_fullchain.save()

        try:
            data_privkey = WhWebsiteVars.objects.get(
                website=bundle.related_obj.id, name="tls_key"
            )
        except WhWebsiteVars.DoesNotExist:
            data_privkey = WhWebsiteVars(website=bundle.related_obj, name="tls_key")

        data_privkey.value = bundle.data["data_privkey"]
        data_privkey.save()

        return bundle


class RegistryDomainResource(Resource):
    class Meta:
        queryset = WhRegistryDomains.objects.all()
        limit = 0
        max_limit = 0
        always_return_data = True
        allowed_methods = ["get", "post"]
        serializer = Serializer()
        authentication = ApiAuthCustomerAuthentication()

    def get_resource_uri(self, bundle_or_obj=None, url_name="api_dispatch_list"):
        return

    def obj_get(self, bundle, **kwargs):
        bundle = WhRegistryDomains.objects.get(id=kwargs["pk"])
        return bundle

    def obj_create(self, bundle, **kwargs):
        registry_domain = WhRegistryDomains(
            customer=bundle.request.user,
            domain=idna.encode(bundle.data["domain"].lower()).decode("utf-8"),
        )

        if check_domain_availability(registry_domain.domain):
            registry_domain.status = register_or_transfer_domain(registry_domain.domain)
        else:
            try:
                auth_code = bundle.data["auth_code"]
            except KeyError:
                raise BadRequest("Domain already registered, please specify auth_code!")

            if register_or_transfer_domain(registry_domain.domain, auth_code):
                registry_domain.status = 0
                create_zone_from_template(
                    str(bundle.request.user.id) + ".zone-templates.local",
                    registry_domain.domain,
                )

        registry_domain.save(force_insert=True)

        try:
            notifier = WhRegistryDomainTickets(
                domain_reg=registry_domain,
                email=registry_domain.customer.primary_email,
            )

            notifier.save(force_insert=True)

            notifier.ticket_id = zammad_send(
                notifier_list=WhRegistryDomainTickets.objects.filter(
                    domain_reg=registry_domain
                ),
                subject="Registrierung erfolgreich - Ihre neue Domain: "
                + registry_domain.domain,
                text="Ihre neue Domain "
                + registry_domain.domain
                + " wurde erfolgreich registriert.",
                group=5,
            )

            notifier.save(force_insert=True)
        except:
            pass

        bundle.obj = registry_domain

        return bundle

    def obj_get_list(self, bundle, **kwargs):
        domain_list = list()

        if "domain" in bundle.request.GET:
            try:
                registry_domain = WhRegistryDomains.objects.get(
                    domain=idna.encode(bundle.request.GET["domain"].lower())
                ).decode("utf-8")
            except WhRegistryDomains.DoesNotExist:
                registry_domain = WhRegistryDomains(
                    customer=bundle.request.user,
                    domain=bundle.request.GET["domain"].lower(),
                )

                registry_domain.available = check_domain_availability(
                    domain=registry_domain.domain
                )

            domain_list.append(registry_domain)
        else:
            domain_list = WhRegistryDomains.objects.filter(customer=bundle.request.user)

        return domain_list

    def dehydrate(self, bundle):
        bundle.data = model_to_dict(bundle.obj)
        bundle.data["get_tld"] = bundle.obj.get_tld
        bundle.data["status_text"] = bundle.obj.get_status_display()

        try:
            bundle.data["available"] = bundle.obj.available
        except AttributeError:
            pass

        return bundle


class TldResource(Resource):
    class Meta:
        queryset = WhIanaTld.objects.all()
        limit = 0
        max_limit = 0
        always_return_data = True
        allowed_methods = ["get"]
        serializer = Serializer()
        authentication = ApiAuthCustomerAuthentication()

    def get_resource_uri(self, bundle_or_obj=None, url_name="api_dispatch_list"):
        return

    def obj_get_list(self, bundle, **kwargs):
        return WhIanaTld.objects.all()

    def dehydrate(self, bundle):
        bundle.data = model_to_dict(bundle.obj)
        return bundle


class DnsZoneResource(Resource):
    class Meta:
        queryset = Domains.objects.none()
        limit = 0
        max_limit = 0
        always_return_data = True
        allowed_methods = ["get", "post", "put", "patch"]
        serializer = Serializer()
        authentication = ApiAuthCustomerAuthentication()

    def get_resource_uri(self, bundle_or_obj=None, url_name="api_dispatch_list"):
        return

    def obj_get(self, bundle, **kwargs):
        dns_domain = idna.encode(kwargs["pk"]).decode("utf-8")

        if dns_domain not in get_own_dns_zones(bundle.request.user):
            raise Unauthorized

        dns_zone = Domains.objects.get(name=dns_domain)

        bundle = dns_zone

        return bundle

    def obj_get_list(self, bundle, **kwargs):
        return get_own_dns_zones(bundle.request.user)

    def obj_create(self, bundle, **kwargs):
        # normalize domain
        domain_orig = idna.encode(bundle.data["domain"].lower()).decode("utf-8")

        create_zone_from_template(bundle.data["template"], bundle.data["domain"])

        bundle.obj = Domains.objects.get(name=domain_orig)

        return bundle

    def obj_update(self, bundle, **kwargs):
        dns_domain = bundle.data["name"]

        if dns_domain not in get_own_dns_zones(bundle.request.user):
            raise Unauthorized

        dns_zone = Domains.objects.get(name=dns_domain)

        response = replace2_on_powerdns_api(
            endpoint="zones/" + dns_domain,
            server_id="localhost",
            data={
                "rrsets": bundle.data["rrsets"],
            },
        )

        if response is not True:
            if "error" in response:
                raise UnsupportedFormat(msg=response["error"])
            else:
                raise UnsupportedFormat

        bundle.obj = dns_zone

        return bundle

    def dehydrate(self, bundle):
        simple_mode = False

        if isinstance(bundle.obj, str):
            simple_mode = True

        bundle.data = dict()

        if simple_mode:
            bundle.data["name"] = bundle.obj
        else:
            bundle.data["name"] = bundle.obj.name

        domain_data = Domains.objects.get(name=bundle.data["name"])

        bundle.data["id"] = domain_data.id

        bundle.data["anycast_enabled"] = False

        try:
            anycast_metadata = domain_data.domainmetadata_set.get(
                kind="X-ANYCAST-ENABLED"
            )
        except Domainmetadata.DoesNotExist:
            pass
        else:
            if anycast_metadata.content == "1":
                bundle.data["anycast_enabled"] = True

        soa = domain_data.records_set.get(
            type="SOA", name=bundle.data["name"]
        ).content.split(" ")

        bundle.data["primary_ns"] = soa[0]
        bundle.data["admin_email"] = soa[1]
        bundle.data["serial"] = soa[2]
        bundle.data["refresh"] = soa[3]
        bundle.data["retry"] = soa[4]
        bundle.data["expire"] = soa[5]
        bundle.data["minimum"] = soa[6]

        if simple_mode:
            return bundle

        pdns_data = get_from_powerdns_api("zones", "localhost", bundle.obj.name)

        if "rrsets" in pdns_data:
            bundle.data["records"] = pdns_data["rrsets"]

        return bundle


class DnsZoneTemplateResource(Resource):
    class Meta:
        queryset = Domains.objects.none()
        limit = 0
        max_limit = 0
        always_return_data = True
        allowed_methods = ["get"]
        serializer = Serializer()
        authentication = ApiAuthCustomerAuthentication()

    def get_resource_uri(self, bundle_or_obj=None, url_name="api_dispatch_list"):
        return

    def obj_get_list(self, bundle, **kwargs):
        return get_own_dns_templates(bundle.request.user)

    def dehydrate(self, bundle):
        bundle.data = dict()

        bundle.data["name"] = bundle.obj

        name_split = bundle.data["name"].split(".")
        id_split = name_split[0].split("-")

        bundle.data["name"] = bundle.obj
        bundle.data["short_name"] = id_split[1]

        domain_data = Domains.objects.get(name=bundle.data["name"])

        bundle.data["id"] = domain_data.id

        return bundle


class WebsiteResource(Resource):
    class Meta:
        queryset = None
        limit = 0
        max_limit = 0
        always_return_data = True
        allowed_methods = ["get"]
        serializer = Serializer()
        authentication = ApiAuthCustomerAuthentication()

    def get_resource_uri(self, bundle_or_obj=None, url_name="api_dispatch_list"):
        return

    def obj_get_list(self, bundle, **kwargs):
        all_websites = WhWebsites.objects.filter(customer=bundle.request.user)

        return all_websites

    def dehydrate(self, bundle):
        bundle.data = model_to_dict(bundle.obj)

        bundle.data["aliases"] = list()

        for alias in bundle.obj.whdomains_set.all():
            bundle.data["aliases"].append(model_to_dict(alias))

        return bundle


class ClusterResource(Resource):
    class Meta:
        queryset = WhClusters.objects.all()
        limit = 0
        max_limit = 0
        always_return_data = True
        allowed_methods = ["get"]
        serializer = Serializer()
        authentication = ApiAuthCustomerAuthentication()

    def get_resource_uri(self, bundle_or_obj=None, url_name="api_dispatch_list"):
        return

    def obj_get_list(self, bundle, **kwargs):
        all_clusters = WhClusters.objects.all()

        return all_clusters

    def dehydrate(self, bundle):
        bundle.data = model_to_dict(bundle.obj)
        return bundle


class HostResource(Resource):
    class Meta:
        queryset = WhWebsites.objects.all()
        limit = 0
        max_limit = 0
        always_return_data = True
        allowed_methods = ["get"]
        serializer = Serializer()
        authentication = ApiAuthCustomerAuthentication()

    def get_resource_uri(self, bundle_or_obj=None, url_name="api_dispatch_list"):
        return

    def obj_get(self, bundle, **kwargs):
        try:
            bundle.obj = WhClusters.objects.get(id=kwargs["pk"])
        except WhClusters.DoesNotExist:
            return

        return bundle

    def dehydrate(self, bundle):
        if bundle.request.method == "GET":
            bundle.data = dict()

            for website in list(
                bundle.obj.obj.whwebsites_set.filter(
                    id=bundle.request.GET["website_id"]
                )
            ):
                bundle.data[website.id] = dict()
                bundle.data[website.id]["domains"] = dict()
                bundle.data[website.id]["params"] = get_web_vars(website)
                bundle.data[website.id]["httpfront_nginx"] = get_httpfront_nginx(
                    website
                )

                for domain in website.whdomains_set.all():
                    bundle.data[website.id]["domains"][domain.id] = get_dom_vars(domain)

                if len(bundle.data[website.id]["domains"]) <= 0:
                    continue

        return bundle
